#! python
import os
import sys
import argparse
import logging
import mft_lib.mft

parser = argparse.ArgumentParser(description='Parses MFT records')
parser.add_argument('path', type=str,
                    help='Path to MFT.')
parser.add_argument('--debug', action="store_true",
                    help='Turn on debug mode.')
args = parser.parse_args()

if args.debug:
    logging.basicConfig(level=logging.DEBUG)


if not os.path.exists(args.path):
    print('File "{}" not found. Quitting...'.format(args.path))
    sys.exit(-1)
else:
    f = open(args.path, 'rb')
    data = f.read()
    logging.debug('File length: {} bytes'.format(len(data)))

logging.debug('Loading MFT...')
m = mft_lib.mft.MFT(data)
for each in m.records:
    if each.full_path:
        print(each.full_path)


#print(m.get_record_by_name('/Users'))