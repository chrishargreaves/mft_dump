from distutils.core import setup
setup(name='mft_dump',
      version='0.2',
      description='MFT_Dump',
      author='Chris Hargreaves',
      author_email='chris@hargs.co.uk',
      scripts=['mft_dump.py', 'mft_record_dump.py'],
      py_modules=['mft_lib.mft',
                  'mft_lib.mft_record',
                ],
      )
