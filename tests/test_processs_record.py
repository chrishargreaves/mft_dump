__author__ = 'chris'

import unittest
import mft_lib.mft_record

class MyTestCase(unittest.TestCase):

    def test_record_size_support(self):
        self.assertRaises(ValueError, mft_lib.mft_record.MFTRecord, b" ")


    def test_type_input_check(self):
        self.assertRaises(ValueError, mft_lib.mft_record.MFTRecord, 4096 * " ")
        self.assertRaises(ValueError, mft_lib.mft_record.MFTRecord, 1)
        self.assertRaises(ValueError, mft_lib.mft_record.MFTRecord, 1.0)
        self.assertRaises(ValueError, mft_lib.mft_record.MFTRecord, {})
        self.assertRaises(ValueError, mft_lib.mft_record.MFTRecord, [])

    def test_FILE_sanity_check(self):
        self.assertRaises(ValueError, mft_lib.mft_record.MFTRecord, b"AAAA" + b" " * 1020)
        self.assertRaises(ValueError, mft_lib.mft_record.MFTRecord, b"BAAD" + b" " * 1020)

    def test_results(self):
        f = open("sample_mft_scarlet.bin", 'rb')
        data = f.read()
        f.close()
        results = mft_lib.mft_record.MFTRecord(data)

        self.assertEqual(results.values['offset_to_fixup'], 48)
        self.assertEqual(results.values['no_values_in_fixup'], 3)
        self.assertEqual(results.values['lsn'], 1088410)
        self.assertEqual(results.values['record_reuse'], 2)
        self.assertEqual(results.values['hard_link_count'], 1)
        self.assertEqual(results.values['offset_to_first_attribute'], 56)
        #todo flags
        self.assertEqual(results.values['used_size_of_mft'], 344)
        self.assertEqual(results.values['allocated_size_of_mft'], 1024)
        self.assertEqual(results.values['file_ref_to_base_count'], 0)
        self.assertEqual(results.values['next_attribute_id'], 4)
        self.assertEqual(results.values['record_id'], 36)


    def test_results_multiple_filename_attributes(self):
        f = open("filename_weird.bin", 'rb')
        data = f.read()
        f.close()
        results = mft_lib.mft_record.MFTRecord(data)
        self.assertEqual(results.filename, 'b.cook')




if __name__ == '__main__':
    unittest.main()
