import struct
import logging
import datetime

class AttributeParser(object):
    def __init__(self, data):
        self.data = data

    def __iter__(self):
        self.pos = 0
        return self

    def __next__(self):
        """Returns the next record"""
        att_header = self.data[self.pos:self.pos+4]

        if att_header == b"\xff\xff\xff\xff" or self.pos > (len(self.data)-4):
            raise StopIteration
        else:
            att_length = struct.unpack("<I", self.data[self.pos+4:self.pos+8])[0]
            if att_length > 0:
                old_pos = self.pos
                self.pos = self.pos + att_length
                return self.data[old_pos:old_pos + att_length]
            else:
                raise StopIteration


class MFTRecord(object):

    def __init__(self, data):
        self.values = self._process_mft_record(data)

    def _process_mft_record(self, data):
        """Processes supplied data and returns a dictionary contained extracted values"""

        self.__check_input_is_bytes(data)
        self.__check_record_length(data)

        if data[0:4] != b'FILE':
            raise(ValueError('Invalid MFT record. No FILE signature'))

        return self.__get_values_from_mft_record(data)

    def __str__(self):
        """Return string representation of the last results"""
        out_str = "\n"
        out_str += "Basic Record Info\n"
        out_str += "=================\n"
        out_str += "Signature: {}\n".format(self.values['signature'])
        out_str += "Offset to fixup array: {}\n".format(self.values['offset_to_fixup'])
        out_str += "No. values in fixup: {}\n".format(self.values['no_values_in_fixup'])
        out_str += "Logfile Sequence Number: {}\n".format(self.values['lsn'])
        out_str += "Record Reuse: {}\n".format(self.values['record_reuse'])
        out_str += "Hard link count: {}\n".format(self.values['hard_link_count'])
        out_str += "Offset to first attribute: {}\n".format(self.values['offset_to_first_attribute'])
        out_str += "Is directory: {}\n".format(self.values['is_directory'])
        out_str += "Used size of MFT record: {}\n".format(self.values['used_size_of_mft'])
        out_str += "Allocated size of MFT record: {}\n".format(self.values['allocated_size_of_mft'])
        out_str += "file_ref_to_base_count: {}\n".format(self.values['file_ref_to_base_count'])
        out_str += "Next attribute ID: {}\n".format(self.values['next_attribute_id'])
        out_str += "Record ID: {}\n".format(self.values['record_id'])
        out_str += "\n"
        out_str += "SIA\n"
        out_str += "===\n"
        out_str += "SIA_created: {}\n".format(self.values['SIA']['SIA_created'])
        out_str += "SIA_modified: {}\n".format(self.values['SIA']['SIA_modified'])
        out_str += "SIA_entry_modified: {}\n".format(self.values['SIA']['SIA_entry_modified'])
        out_str += "SIA_accessed: {}\n".format(self.values['SIA']['SIA_accessed'])
        out_str += "SIA_flags: {}\n".format(self.values['SIA']['SIA_flags'])
        out_str += "SIA_max_no_versions: {}\n".format(self.values['SIA']['SIA_max_no_versions'])
        out_str += "SIA_class_id: {}\n".format(self.values['SIA']['SIA_class_id'])

        # TODO PUT THIS BACK IN
        # out_str += "SIA_owner_id: {}\n".format(self.values['SIA']['SIA_owner_id'])
        # out_str += "SIA_security_id: {}\n".format(self.values['SIA']['SIA_security_id'])
        # out_str += "SIA_quota_charged: {}\n".format(self.values['SIA']['SIA_quota_charged'])
        # out_str += "SIA_update_sequence_number: {}\n".format(self.values['SIA']['SIA_update_sequence_number'])
        out_str += "\n"
        out_str += "Filename\n"
        out_str += "========\n"
        out_str += "FN_parent_id: {}\n".format(self.values['Filename']['FN_parent_id'])
        out_str += "FN_created: {}\n".format(self.values['Filename']['FN_created'])
        out_str += "FN_modified: {}\n".format(self.values['Filename']['FN_modified'])
        out_str += "FN_entry_modified: {}\n".format(self.values['Filename']['FN_entry_modified'])
        out_str += "FN_accessed: {}\n".format(self.values['Filename']['FN_accessed'])
        out_str += "FN_allocated_file_size: {}\n".format(self.values['Filename']['FN_allocated_file_size'])
        out_str += "FN_real_file_size: {}\n".format(self.values['Filename']['FN_real_file_size'])
        out_str += "FN_flags: {}\n".format(self.values['Filename']['FN_flags'])
        out_str += "FN_reparse_value: {}\n".format(self.values['Filename']['FN_reparse_value'])
        out_str += "FN_file_name_length: {}\n".format(self.values['Filename']['FN_file_name_length'])
        out_str += "FN_namespace: {}\n".format(self.values['Filename']['FN_namespace'])
        out_str += "FN_filename: {}\n".format(self.values['Filename']['FN_filename'])
        return out_str


    def __check_record_length(self, data):
        """"Checks the supplied data is ok for an MFT record"""
        if len(data) != 1024:
            raise ValueError("Unsupported MFT record length")
        else:
            return True

    def __check_input_is_bytes(self, data):
        """"Checks the supplied data is bytes"""
        if type(data) != bytes:
            raise ValueError("Unsupported input. Must be bytes.")
        else:
            return True

    def __get_values_from_mft_record(self, record_data):
        """Extracts values from MFT record into a dictionary"""
        values = {}
        values["signature"] = record_data[0:4]
        values["offset_to_fixup"] = struct.unpack("<B", record_data[4:5])[0]
        values["no_values_in_fixup"] = struct.unpack("<H", record_data[6:8])[0]
        values["lsn"] = struct.unpack("<Q", record_data[8:16])[0]
        values["record_reuse"] = struct.unpack("<H", record_data[16:18])[0]
        values["hard_link_count"] = struct.unpack("<H", record_data[18:20])[0]
        values["offset_to_first_attribute"] = struct.unpack("<H", record_data[20:22])[0]
        values["flags"] = bin(struct.unpack("<H", record_data[22:24])[0])
        if struct.unpack("<H", record_data[22:24])[0] & 0b00000010:
            values["is_directory"] = "True"
        else:
            values["is_directory"] = "False"
        if struct.unpack("<H", record_data[22:24])[0] & 0b00000001:
            values["in_use"] = "True"
        else:
            values["in_use"] = "False"
        values["used_size_of_mft"] = struct.unpack("<I", record_data[24:28])[0]
        values["allocated_size_of_mft"] = struct.unpack("<I", record_data[28:32])[0]
        values["file_ref_to_base_count"] = struct.unpack("<Q", record_data[32:40])[0]
        values["next_attribute_id"] = struct.unpack("<H", record_data[40:42])[0]
        values["record_id"] = struct.unpack("<I", record_data[44:48])[0]

        values['fixup'] = record_data[values["offset_to_fixup"]:
                                      values["offset_to_fixup"] + values["no_values_in_fixup"]]

        # modifies record data with fixup
        record_data = self.__do_fixup(values["offset_to_fixup"], values["no_values_in_fixup"], record_data)

        attribute_parser = AttributeParser(record_data[values["offset_to_first_attribute"]:])

        for each_attribute in attribute_parser:
            res = self.__process_attribute(each_attribute)


            # TODO Fix this bad code properly
            attribute_name = None
            for each in res:
                attribute_name = each

            if attribute_name in values:
                # attribute has already been found
                # how to process?
                # only a problem for filename at the moment so patching
                # TODO Fix this properly
                if attribute_name == 'Filename':
                    if res['Filename'].get('FN_namespace') == 1 or res['Filename'].get('FN_namespace') == 3:
                        # 0=Posix, 1=Win32, 2=DOS, 3=Win32 & DOS
                        values.update(res)
                    else:
                        pass
                        # skip it for now
            else:
                # just update
                values.update(res)

        return values

    def __do_fixup(self, offset_to_fixup, no_values, record_data):
        """Updates the values in the MFT record using the fixup array"""

        if no_values != 3:
            raise Exception("Not 3 values in fixup (there are %d). Don't know what to do with that" % no_values)

        fixup_check = record_data[offset_to_fixup:offset_to_fixup+2]
        fixup1 = record_data[offset_to_fixup+2:offset_to_fixup+4]
        fixup2 = record_data[offset_to_fixup+4:offset_to_fixup+6]

        # Checks first fixup value
        if record_data[510:512] != fixup_check:
            print(fixup_check)
            print(fixup1)
            print(record_data[510:512])
            raise Exception("Error in 1st fixup array")

        # Checks second fixup value
        if record_data[1022:1024] != fixup_check:
            print(fixup_check)
            print(fixup2)
            print(record_data[1022:1024])
            raise Exception("Error in 1st fixup array")

        temp_data = record_data[0:510]
        temp_data += fixup1
        temp_data += record_data[512:1022]
        temp_data += fixup2
        record_data = temp_data
        return record_data

    def __process_attribute(self, attribute_data):
        """Determine type of attribute and return values"""
        logging.debug('Processing attribute {}'.format(str(attribute_data[0:4])))
        offset_to_stream = struct.unpack("<H", attribute_data[20:22])[0]

        if attribute_data[0:4] == b"\x10\x00\x00\x00": # Found SIA attribute
            res = {"SIA": self.__process_sia_attribute(attribute_data[offset_to_stream:])}
            return res
        elif attribute_data[0:4] == b"\x30\x00\x00\x00": # Found FileName attribute
            res = {"Filename": self.__process_file_name_attribute(attribute_data[offset_to_stream:])}
            return res
        elif attribute_data[0:4] == b"\x80\x00\x00\x00": # Found FileName attribute
            return {}
        elif attribute_data[0:4] == b"\xB0\x00\x00\x00": # Found FileName attribute
            return {}
        else:
            return {}


    def __process_sia_attribute(self, attribute_data):
        values = {}
        values["SIA_created_value"] = struct.unpack("<Q", attribute_data[0:8])[0]
        values["SIA_created"] = self.__convert_windows_time(values["SIA_created_value"])
        values["SIA_modified_value"] = struct.unpack("<Q", attribute_data[8:16])[0]
        values["SIA_modified"] = self.__convert_windows_time(values["SIA_modified_value"])
        values["SIA_entry_modified_value"] = struct.unpack("<Q", attribute_data[16:24])[0]
        values["SIA_entry_modified"] = self.__convert_windows_time(values["SIA_entry_modified_value"])
        values["SIA_accessed_value"] = struct.unpack("<Q", attribute_data[24:32])[0]
        values["SIA_accessed"] = self.__convert_windows_time(values["SIA_accessed_value"])
        values["SIA_flags"] = attribute_data[32:36]
        values["SIA_max_no_versions"] = struct.unpack("<I", attribute_data[36:40])[0]
        values["SIA_class_id"] = struct.unpack("<Q", attribute_data[40:48])[0]
        # values["SIA_owner_id"] = struct.unpack("<I", attribute_data[48:52])[0]
        # values["SIA_security_id"] = struct.unpack("<I", attribute_data[52:56])[0]
        # values["SIA_quota_charged"] = struct.unpack("Q", attribute_data[56:64])[0]
        # values["SIA_update_sequence_number"] = struct.unpack("<Q", attribute_data[64:72])[0]
        return values

    def __process_file_name_attribute(self, attribute_data):
        values = {}
        values["FN_parent_id"] = struct.unpack("<I", attribute_data[0:4])[0] # 8 in Sammes, 6 in Carrier
        values["FN_created_value"] = struct.unpack("<Q", attribute_data[8:16])[0]
        values["FN_created"] = self.__convert_windows_time(values["FN_created_value"])
        values["FN_modified_value"] = struct.unpack("<Q", attribute_data[16:24])[0]
        values["FN_modified"] = self.__convert_windows_time(values["FN_modified_value"])
        values["FN_entry_modified_value"] = struct.unpack("<Q", attribute_data[24:32])[0]
        values["FN_entry_modified"] = self.__convert_windows_time(values["FN_entry_modified_value"])
        values["FN_accessed_value"] = struct.unpack("<Q", attribute_data[32:40])[0]
        values["FN_accessed"] = self.__convert_windows_time(values["FN_accessed_value"])
        values["FN_allocated_file_size"] = struct.unpack("<Q", attribute_data[40:48])[0]
        values["FN_real_file_size"] = struct.unpack("<Q", attribute_data[48:56])[0]
        values["FN_flags"] = struct.unpack("<I", attribute_data[56:60])[0]
        values["FN_reparse_value"] = struct.unpack("<I", attribute_data[60:64])[0]
        values["FN_file_name_length"] = struct.unpack("B", attribute_data[64:65])[0]
        values["FN_namespace"] = struct.unpack("B", attribute_data[65:66])[0]
        values["FN_name_data"] = attribute_data[66:66 + values["FN_file_name_length"] * 2]
        values["FN_filename"] = values["FN_name_data"].decode("UTF16")
        return values

    def __convert_windows_time(self, time_to_convert):
        """Converts a Windows time to Unix time unless timestamp is 0, then it returns 0"""

        if time_to_convert <= -1:
            logging.error("Cannot convert negative WindowsTime")
            raise ValueError("Cannot convert negative WindowsTime")
        if time_to_convert == 0:
            return '0000-00-00 00:00:00'
        else:
            try:
                timestamp_data = time_to_convert - 116444736000000000
                unix_timestamp = timestamp_data/10000000
                time_object = datetime.datetime.utcfromtimestamp(unix_timestamp)
                return datetime.datetime.isoformat(time_object, sep=' ')
            except OverflowError:
                logging.warning('Overflow error converting time {}. Setting to all zeros'.format(time_to_convert))
                return '0000-00-00 00:00:00'

    # Bunch of properties for quick access to particular properties

    @property
    def created_time(self):
        if not self.values.get('SIA'):
            return None
        return self.values.get('SIA').get('SIA_created')

    @property
    def modified_time(self):
        if not self.values.get('SIA'):
            return None
        return self.values.get('SIA').get('SIA_modified')

    @property
    def full_path(self):
        return self.values.get('full_path')

    @property
    def filename(self):
        if not self.values.get('Filename'):
            return None
        if not self.values.get('Filename').get('FN_filename'):
            return None

        return self.values.get('Filename').get('FN_filename')

