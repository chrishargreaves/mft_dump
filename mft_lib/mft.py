import mft_lib.mft_record
import logging
import re

class MFT(object):

    def __init__(self, data):
        self.data = data
        self.record_size = 1024
        self.records = self._process_records()
        self.filename_index = {}

        # update with full_paths
        for each in self.records:
            full_path = self.follow_ids(each.values['id'])
            if full_path:
                each.values['full_path'] = full_path
                self.filename_index[full_path] = each.values['id']
            else:
                pass
                #print('No path for {}'.format(each.filename))
                #print(each)

    def get_record_by_name(self, record_name):
        id = self.filename_index.get(record_name)
        if not id:
            return None
        else:
            return self.records[id]

    def _process_records(self):
        current_offset = 0
        current_id = 0
        records = []
        while current_offset < len(self.data):
            retrieved_record = self._process_record(current_id)
            if retrieved_record != None:
                records.append(retrieved_record)

            current_id += 1
            current_offset += self.record_size
        return records


    def _process_record(self, id):
        """Get values for a particular record"""
        logging.debug("Fetching record %d " % id)
        offset = id * self.record_size
        logging.debug("offset = %d" % offset)
        record_data = self.data[offset:offset+self.record_size]
        if record_data[0:4] == b"FILE": # this should probably check for all zeros to be sure.
            mft_record = mft_lib.mft_record.MFTRecord(record_data)
            mft_record.values["id"] = id
            mft_record.values["offset"] = id * self.record_size
            return mft_record
        else:
            return None

    def follow_ids(self, id):
        path = ""
        this_record = self.records[id]

        fn_res = this_record.values.get('Filename')

        if fn_res and fn_res.get("FN_parent_id") and fn_res.get("FN_filename"):
            parent_id = fn_res["FN_parent_id"]
            if id != 5:  # 5 is always root folder
                parent_record = self.records[parent_id]
                if parent_record.values["is_directory"] == "False":
                    # the parent is a file and the chain is broken, put in orphan
                    path = "/$OrphanFiles"
                    return path + "/" + fn_res["FN_filename"]
                else:
                    path = self.follow_ids(parent_id)
                    if path is not None:
                        path += "/" + fn_res["FN_filename"]
                    else:
                        path = 'UNKNOWN/' + fn_res["FN_filename"]
            else:
                return ''
        else:
            return None

        return path